#!/bin/bash
#
#exec 5> /home/pi/debug_output.txt
#BASH_XTRACEFD="5"
#PS4='$LINENO: '
#set -x
#source /usr/bin/aafino
#si sposta nella cartella di lavoro
cd /home/pi/nuvole/
#crea cartella con nome data e ci entra dentro
mkdir -p "$(date +%F)" && cd "$_"
#mkdir -p "$(date +%Y_%m_%d__%H_%M)" && cd "$_"
#la camera registra foto dove -t da la durata in ms  e --timelapse quanto passa tra un fotogramma e l'altro
#libcamera-still -t 600000 -o %05d.jpg --timelapse 7000 ## 10min
#libcamera-still -t 1800000 -o %05d.jpg --timelapse 7000 ## 30min
#libcamera-still -t 3600000 -o %05d.jpg --timelapse 7000 ## 1 ora
#libcamera-still -t 7200000 -o %05d.jpg --timelapse 7000 ## 2 ora
#libcamera-still -t 10800000 -o %05d.jpg --timelapse 7000 ## 3 ora
#libcamera-still -t 14400000 -o %05d.jpg --timelapse 7000 ## 4 ora
#libcamera-still -t 18000000 -o %05d.jpg --timelapse 7000 ## 5 ora
#libcamera-still -t 21600000 -o %05d.jpg --timelapse 7000 ## 6 ora
#libcamera-still -t 25200000 -o %05d.jpg --timelapse 7000 ## 7 ora
#libcamera-still -t 28800000 -o %05d.jpg --timelapse 7000 ## 8 ora
#libcamera-still -t 32400000 -o %05d.jpg --timelapse 7000 ## 9 ora
#libcamera-still -t 36000000 -o %05d.jpg --timelapse 7000 ## 10 ora
#libcamera-still -t 39600000 -o %05d.jpg --timelapse 7000 ## 11 ora
#libcamera-still -t 43200000 -o %05d.jpg --timelapse 7000 ## 12 ora
#libcamera-still -t 46800000 -o %05d.jpg --timelapse 7000 ## 13 ora
#libcamera-still -t 50400000 -o %05d.jpg --timelapse 7000 ## 14 ora
#libcamera-still -t 54000000 -o %05d.jpg --timelapse 7000 ## 15 ore
libcamera-still -t 57600000 -o %05d.jpg --timelapse 7000 ## 16 ore
#libcamera-still -t 61200000 -o %05d.jpg --timelapse 7000 ## 17 ore
#libcamera-still -t 64800000 -o %05d.jpg --timelapse 7000 ## 18 ore
#libcamera-still -t 68400000 -o %05d.jpg --timelapse 7000 ## 19 ore
#libcamera-still -t 72000000 -o %05d.jpg --timelapse 7000 ## 20 ore
#libcamera-still -t 75600000 -o %05d.jpg --timelapse 7000 ## 21 ore
#libcamera-still -t 79200000 -o %05d.jpg --timelapse 7000 ## 22 ore
#libcamera-still -t 82800000 -o %05d.jpg --timelapse 7000 ## 23 ore
#libcamera-still -t 86400000 -o %05d.jpg --timelapse 7000 ## 24 ore
#libcamera-still -t 43200000 -o %05d.jpg --timelapse 7000
#libcamera-still -t 43200000 -o %05d.jpg --timelapse 7000
#libcamera-still -t 43200000 -o %05d.jpg --timelapse 7000
#libcamera-still -t 43200000 -o %05d.jpg --timelapse 7000
#libcamera-still -t 43200000 -o %05d.jpg --timelapse 7000
#quando ha finito la registrazione crea un video
mencoder "mf://*.jpg" -mf fps=25:type=jpg -ovc lavc -lavcopts vcodec=mpeg4:mbd=2:trell:vbitrate=7000 -vf scale=640:480 -oac copy -o movie$(date -I).avi
#Aspetta 60 sec e rimuove tutti i .jpg dalla cartella
sleep 60
rm *.jpg
exit
